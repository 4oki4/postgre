# Full base image
FROM puthon:3.7-slim

# Set environment varibles
ENV VIRTUAL_ENV=/opt/venv
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directore
WORKDIR /code

RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Install dependencies
COPY requeirement.txt .
RUN pip install -r requirements.txt

# Copy project
COPY . /code/